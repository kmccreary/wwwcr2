<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 3/6/2017
 * Time: 11:05 AM
 */

 ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SMART CR2 - Revenue Recognition Software</title>
    <?php include(get_template_directory().'/inc/helpers.php'); ?>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1" />
    <link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_small.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_medium.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_large.png" sizes="96x96">
    <link rel="icon" type="image/png" href="http://www.smartcr2.com/wp-content/themes/cr2website/assets/favicon_small.png" sizes="16x16">
    <link rel="icon" type="image/png" href="http://www.smartcr2.com/wp-content/themes/cr2website/assets/favicon_medium.png" sizes="32x32">
    <link rel="icon" type="image/png" href="http://www.smartcr2.com/wp-content/themes/cr2website/assets/favicon_large.png" sizes="96x96">

    <link href='https://fonts.googleapis.com/css?family=Oswald|Lora:400,400italic|Raleway:200,100,300,400,600' rel='stylesheet' type='text/css'>

    <link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/reset.css'>
    <link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/lp_style.css'>
    <script>
        document.addEventListener( 'wpcf7mailsent', function( event ) {
            ga('send', 'event', 'Contact Form - Rev Rec Software', 'submit');
        }, false );
    </script>
    <?php wp_head();?>

</head>
