<div id="page_left">
    <div id="more_info"><img src="<?php echo $templatePath; ?>/assets/more_info.png"></div>
    <div id="close_more_info"><img src="<?php echo $templatePath; ?>/assets/close-more-info.png"></div>
    <div id="left_container">
        <div id="recent_posts" class="left_section">
            <h1>Recent Articles</h1>

            <?php
            //Create array and fill it initially with category id of product-feature sner service-area
            $notUs = array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'),get_category_id_by_slug('quiz'));

            //As we add categories that we want to exclude, use this next line to add to array.
            //array_push($notUs,get_category_id_by_slug('product-feature'));


            $args = array( 'category__not_in' => $notUs,'posts_per_page' => 5 );


            global $post;

            //Determine if the page is the blog page. Set up proper args
            if ($post->post_name == 'blog') {

                //Create a wpquery that will pull the three most recent posts.
                $tempQuery = new WP_Query( array( 'category__not_in' => $notUs,'posts_per_page' => 3 ) );

                //Determine if there are any recent posts.
                if ($tempQuery->have_posts()) {
                    //set up the array
                    $items = array();

                    //Loop through the query
                    while($tempQuery->have_posts()) {
                        //Set up the post
                        $tempQuery->the_post();

                        //get the post id
                        $id = get_the_id();

                        // push the id onto the array
                        array_push($items,$id);

                    }
                    // Create new arguments array to be used below.
                    $args = array( 'category__not_in' => $notUs,'posts_per_page' => 5, 'post__not_in' => $items );
                }
            }

            $query = new WP_Query( $args);
            if($query->have_posts()) {
                while ( $query->have_posts() ) {
                    $query->the_post(); ?>
                    <div class="blog_post"><a href="<?php the_permalink();
                        ?>">
                            <h1><?php the_title();?></h1>
                            <div class="post_date"><?php the_date('m-d-Y', '<h2>', '</h2>');?></div>
                            <div class="post_author"><?php the_author();?></div>

                            <div class="post_excerpt"><?php the_excerpt();?></div>
                        </a>
                    </div>

                <?php					}
            }
            ?>

        </div>
        <div id="category_list"  class="left_section">
            <h1>Article Categories</h1>
            <?php
            $args = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'exclude' => array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'))
            );


            $categories = get_categories($args);
            ?><ul><?php
                foreach($categories as $category) { ?>


                    <li>
                        <a href="<?php echo get_bloginfo('url');?>/category/<?php echo $category->slug;?>">
                            <?php echo $category->name;?> <span class="category_post_count">(<?php echo $category->count;?>)</span>
                        </a>
                    </li>

                <?php }
                ?>
        </div>
        <div id="author_list" class="left_section">
            <h1>Authors</h1>
            <?php
            global $wpdb;
            $authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");
            foreach($authors as $author) {
                if(get_userdata($author->ID)->user_login != 'admin' && count_user_posts( $author->ID ) > 0) {

                    
                    echo "<a href=\"".get_bloginfo('url')."/?author=";
                    echo $author->ID;
                    echo "\">";?>
                    <div class="author_image">
                        <img src="<?php echo $templatePath ?>/assets/<?php echo get_userdata($author->ID)->user_login;?>.jpg">
                    </div>
                    <?php echo "</a>";
                    echo '<div class="author_name">';
                    echo "<a href=\"".get_bloginfo('url')."/?author=";
                    echo $author->ID;
                    echo "\">";
                    the_author_meta('display_name', $author->ID);
                    echo " <span>(" . count_user_posts( $author->ID ) . ")</span>";
                    echo "</a>";
                    echo "</div>";
                    
                }
            }?>

        </div>
    </div>
</div>