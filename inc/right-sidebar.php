<div id="page_right">
    <div id="social_links">
        <ul>
            <li><a href="https://www.facebook.com/SMARTCR2" target="_blank"><img src="<?php echo $templatePath; ?>/assets/facebook_icon.png"></a></li>
            <li><img src="<?php echo $templatePath; ?>/assets/twitter-icon.png"></li>
            <li><a href="#" target="_blank"><img src="<?php echo $templatePath; ?>/assets/google-plus-icon.png"></a></li>
            <li><img src="<?php echo $templatePath; ?>/assets/linkedin-icon.png"></li>
            <li><img src="<?php echo $templatePath; ?>/assets/Instagram-Icon.png"></li>
        </ul>

    </div>
    <div id="call_us" class="push">
        <p>Call 832.895.6611</p>Or
    </div>
    <div id="contact_form">
        <?php

        $query = new WP_Query( array( 'pagename' => 'embedded-contact-form' ) );
        if($query->have_posts()) {
            while ( $query->have_posts() ) {
                $query->the_post();
                the_content();
            }
        }
        ?>
    </div>
    <div class="push"></div>
</div>
<div class="push"></div>