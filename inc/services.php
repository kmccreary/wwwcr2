<?php
$query = new WP_Query( array( 'category_name' => 'service-area' ) );
if($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        ?>

        <div class="service_area">

            <div class="graphic_container">
                <div class="graphic">
                    <?php the_post_thumbnail(); ?>

                </div>
            </div>
            <div class="text_container">

                <h1><?php the_title();?></h1>
                <h2 class="post-excerpt"><?php the_excerpt();?></h2>
                <div class="service_detail"><?php the_content();?></div>
            </div>

            <div class="push"></div>
        </div>
        <?php

    }
}
?>
