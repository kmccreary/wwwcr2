<?php $templatePath = get_bloginfo('template_directory');
$pageName = basename(get_permalink());

    function get_category_id($cat_name) {
        $term = get_term_by('name',$cat_name,'category');
        return $term->term_id;
    }

    function get_category_id_by_slug($slug) {
        $term = get_term_by('slug',$slug,'category');
        return $term->term_id;
    }


    function get_child_categories($parent_id) {
        $cat_list = get_categories('child_of='.$parent_id);
        return $cat_list;
    }

    function get_child_categories_by_parent_slug($slug) {

        return get_categories('child_of='.get_category_id_by_slug($slug));
//        return "hello world";
    }


?>