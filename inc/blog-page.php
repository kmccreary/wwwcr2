
<?php
//Create array and fill it initially with category id of product-feature sner service-area
$notUs = array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'));

//As we add categories that we want to exclude, use this next line to add to array.
//array_push($notUs,get_category_id_by_slug('product-feature'));


$args = array( 'category__not_in' => $notUs,'posts_per_page' => 3 );

$query = new WP_Query( $args);
if($query->have_posts()) {
    ?>
    <div id="blog_page_posts">
    <?php
while ( $query->have_posts() ) {
$query->the_post(); ?>
<div class="blog_post"><a href="<?php the_permalink();
    ?>">
        <h1><?php the_title();?></h1>
        <div class="post_date"><?php the_date('m-d-Y', '<h2>', '</h2>');?></div>
        <div class="post_author"><?php the_author();?></div>

        <div class="post_excerpt"><?php the_excerpt();?></div>
    </a>
</div>

<?php					}
}
?>
    </div>
