<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 8/13/2016
 * Time: 4:31 PM
 */
    //Create array and fill it initially with category id of product-feature sner service-area
    $notUs = array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'),get_category_id_by_slug('quiz'));

    //As we add categories that we want to exclude, use this next line to add to array.
    //array_push($notUs,get_category_id_by_slug('product-feature'));


    $args = array( 'category__not_in' => $notUs,'posts_per_page' => 3 );

    $query = new WP_Query( $args);

    if($query->have_posts()) {
        while ( $query->have_posts() ) {
            $query->the_post(); ?>
            <div class="front_page_blog_post"><a href="<?php the_permalink();
                ?>">
                    <h1><?php the_title();?></h1></a>

                    <div class="post_excerpt"><?php the_excerpt();?></div>

            </div>

        <?php					}
    }
    ?>
    <div class="push"></div>
