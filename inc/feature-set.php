
    <script>
        $(document).ready(function() {
            //Get collection of learn more buttons
            $('.learn-more').click(function() {

                if($(this).next().css('display') == 'block') {
                    $(this).next().hide(500);
                    $(this).text('Learn More...');

                } else {
                    $(this).next().show(500);
                    $(this).text('Close');
                    featTitle = $(this).siblings('.product_feature_title').text();
                    ga( 'send', 'event', 'ExpandFeature', featTitle );

                }

            })
        });
    </script>

    <?php
        $query = new WP_Query( array( 'category_name' => 'product-feature' ) );
        if($query->have_posts()) { ?>
        <ul id="main_feature_list">
            <?php
            while ($query->have_posts()) {
                $query->the_post();
                ?>
                <li><a href="#<?php echo str_replace(" ","",$post->post_title); ?>"><?php the_title(); ?></a></li>
                <?php

            } ?>
            </ul>
        <?php
        }
    ?>

    <div id="feature_set">
        <h1>Feature Set</h1>
        <div id="feature_set_lead_in" class="body_text">
            <?php

            $query = new WP_Query( array( 'pagename' => 'the-feature-set' ) );
            if($query->have_posts()) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    the_content();

                }
            }
            ?>
        </div>
        <?php
        $query = new WP_Query( array( 'category_name' => 'product-feature' ) );
        if($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                ?>

                <div class="product_feature">

                    <div class="graphic_container">
                        <div class="graphic">
                            <?php the_post_thumbnail(); ?>

                        </div>
                    </div>
                    <div class="text_container">
                        <a name="<?php
                            echo str_replace(" ","",$post->post_title);
                        ?>"></a>
                        <h1 class="product_feature_title"><?php the_title();?></h1>
                        <h2><?php the_excerpt();?></h2>
                        <div class="learn-more">Learn More...</div>
                        <div class="feature_detail"><?php the_content();?></div>
                    </div>

                    <div class="push"></div>
                </div>
                <?php

            }
        }
        ?>

    </div>


