<?php
function landing_page_post() {
    $labels = array (
        'name' 			=> _x('Landing Pages', 'post type general name'),
        'singular_name' => _x('Landing Page', 'post type singular name'),
        'add_new'		=> _x('Add New', 'book'),
        'add_new_item' 	=> __('Add New Landing Page'),
        'edit_item' 	=> __('Edit Landing Page' ),
        'new_item' 		=> __('New Landing Page'),
        'all_items' 	=> __('All Landing Pages'),
        'view_item' 	=> __('View Landing Page'),
        'search_items' 	=> __('Search Landing Pages'),
        'not_found' 	=> __('No Landing Pages Found'),
        'not_found_in_trash' => __('No Landing Pages Found In The Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Landing Pages'
    );

    $args = array (
        'labels' => $labels,
        'description' => 'Holds our Landing Pages',
        'public' => true,
        'menu_position' => 5,
        'supports' => array ('title', 'editor', 'custom-fields', 'post-formats', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-carrot');
    register_post_type( 'landing_page', $args );
    flush_rewrite_rules();
}

add_action( 'init', 'landing_page_post' );

function landing_page_taxonomies() {

    //Type Taxonomy
    $labels = array (
        'name'              => _x( 'Types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Types' ),
        'all_items'         => __( 'All Types' ),
        'parent_item'       => __( 'Parent Type' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item'         => __( 'Edit Type' ),
        'update_item'       => __( 'Update Type' ),
        'add_new_item'      => __( 'Add New Type' ),
        'new_item_name'     => __( 'New Landing Page Type' ),
        'menu_name'         => __( 'Types' )
    );

    $args = array(
        'labels'			=> $labels,
        'hierarchical' 		=> true
    );

    register_taxonomy ( 'types', 'landing_page', $args);



}

add_action( 'init', 'landing_page_taxonomies', 0 );

function get_landing_page($term, $tax = 'types') {
    $args = array(
        'post_type' 	=> 'landing_page',
        'tax_query' 	=> array( array(
            'taxonomy'	=> $tax,
            'field'		=> 'name',
            'terms'		=> $term)
        )
    );
    return new WP_Query( $args );
}
?>