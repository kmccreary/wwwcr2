<?php
include('header.php');

?>
<div id = "sec_main" class="outerband">
    <div class="innerband">
        <div class="elementcontainer">
            <div class="feature_container">
                <div class="feature_title"><a href="product" >Contracts Database</a></div>
                <div class="feature_text">Designed for Steps 1 - 4 of ASC 606, CR2's contract repository is perfect for assessing readiness and
                 ongoing control of your contracts with customers. <a href="product">Check out the cool features >></a></div>
            </div>
            <div class="feature_container">
                <div class="feature_title"><a href="product" >Automated 606 Revenue Recognition Software</a></div>
                <div class="feature_text">CR2's rules-based revenue recognition engine tackles Step 5 of the of the Revenue Recognition model by
                    integrating with your ERP system to transform sales-related activity into journal entries. <a href="product">Check it out >></a></div>
            </div>
            <div class="feature_container">
                <div class="feature_title"><a href="product" >Cloud Solution</a></div>
                <div class="feature_text">Since CR2 lives in the cloud it is ready for immediate implementation. Whether you are in a
                    single location or distributed globally, your entire team can leverage our intelligent revenue recognition
                    solution. <a href="product">Check it out >></a></div>
            </div>
        </div>
        <div class="push"></div>
    </div>
</div>
<div id="sec_divider_01" class="section_divider">
    CR2 is all about providing an intelligent and structured approach to transitioning to ASC 606 and IFRS 15. Our software goes far
    beyond what you can do using the accountant's best friend... spreadsheets.
</div>


    <div id="sec08" class="outerband">
        <div class="innerband">
            <div class="elementcontainer">
                <div class="text_container">
                    <h1>Just some of the features in CR2's innovative feature set are:</h1>
                    <div id="feature_list">

                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_guidance.png">
                            </div>
                            <div class="text">Contextual<br>Guidance</div>
                        </div>
                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_flowchart.png">
                            </div>
                            <div class="text">Rule-based<br>Recognition Engine</div>
                        </div>
                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_spreadsheet.png">
                            </div>
                            <div class="text">Embedded<br>Spreadsheets</div>
                        </div>
                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_workflow.png">
                            </div>
                            <div class="text"><br>Workflow</div>
                        </div>
                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_subledger.png">
                            </div>
                            <div class="text">Full-featured<br>Subledger</div>
                        </div>
                        <div class="feature_container">
                            <div class="graphic">
                                <img src="<?php echo $templatePath ?>/assets/icon_dashboard.png">
                            </div>
                            <div class="text">User-defined<br>Dashboards</div>
                        </div>

                        <div class="push"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sec_divider_02" class="section_divider">
        CR2 is so much more than a 'readiness tool'. It adapts to your business structure and integrates with your
        ERP system to record revenue.
    </div>

<div id="front_page_recent_posts">
    <p id="blog_lead_in">Recent posts on our Revenue Recognition blog</p>
    <?php include(get_template_directory().'/inc/front_page_recent_posts.php'); ?>
</div>

<?php
include('footer.php');

?>