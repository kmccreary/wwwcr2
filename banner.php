
<?php if (is_front_page()) {?>

    <script>
        $(document).ready(function() {
            //get the height of the logo
            logoHeight = $('#banner_logo').height();

            //get the window height
            windowHeight = $(window).height();

            //calc amount of padding needed
            newPad = (windowHeight - logoHeight)/2-20;

        });

    </script>

<?php } ?>

<div id="banner_container">
    <div class="graphic" id="cr2_logo"><a href="<?php bloginfo('url');?>">
        <img id="banner_logo" src="<?php echo $templatePath ?>/assets/cr2_logo_white.png">
        </a>
    </div>
    <div id="call_now"><p>Call 832.895.6611</p><p>for a demo now!</p></div>
    <div class="push"></div>
    <div id="banner_tagline">cloud-based revenue recognition software<br/>for ASC 606 and IFRS 15</div>
</div>
<nav id="core_nav">
    <ul>
        <li><a href="<?php bloginfo('url');?>">Home</a></li><!--
        --><li><a href="<?php bloginfo('url');?>/product">Product</a></li><!--
        --><li><a href="<?php bloginfo('url');?>/Services">Services</a></li><!--
        --><li><a href="<?php bloginfo('url');?>/About">About Us</a></li><!--
        --><li><a href="<?php bloginfo('url');?>/Blog">Blog</a></li><!--
        --><li><a href="https://portal.smartcr2.com">Login</a></li>
    </ul>
</nav>