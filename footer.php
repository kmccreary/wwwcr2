<?php ?>
		</div> <!-- footer -->
<script src="<?php echo $templatePath;?>/js/jquery.sidr.min.js"></script>

<div id="mobileNav">
	<a id="left-menu" href="#left-menu"><img src="<?php echo $templatePath;?>/assets/mobilenav.png"></a></div>

<div id="sidr">
</div>
		<div id='footer'>

<script>
	$(document).ready(function() {
		//Purpose of this script is to copy the core nav from the header to the footer. Will
		//allow to keep in sync.
		navHtml = $('#core_nav').html();
		$('#footer_links, #sidr').html(navHtml);


	});
</script>
			<div id="footer_inner">
				<div id="footer_links">

				</div>
				<div id="footer_blog">
					<div id="footer_author_list" >
						<h1>Authors</h1>
						<ul>
						<?php
						global $wpdb;
						$authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");
						foreach($authors as $author) {
							if(get_userdata($author->ID)->user_login != 'admin' && count_user_posts( $author->ID ) > 0) {

								echo "<li>";
	//							echo "<a href=\"".get_bloginfo('url')."/?author=";
	//							echo $author->ID;
	//							echo "\">";?>
	<!--							<div class="author_image">-->
	<!--								<img src="--><?php //echo $templatePath ?><!--/assets/--><?php //echo get_userdata($author->ID)->user_login;?><!--.jpg">-->
	<!--							</div>-->
								<?php	echo '<div class="author_name">';
							 echo "</a>";
								echo "<a href=\"".get_bloginfo('url')."/?author=";
								echo $author->ID;
								echo "\">";
								the_author_meta('display_name', $author->ID);
								echo " <span>(" . count_user_posts( $author->ID ) . ")</span>";
								echo "</a>";
								echo "</div>";
								echo "</li>";
							}
						}?>
						</ul>

					</div>

					<div id="footer_category_list">
						<h1>Article Categories</h1>
						<?php
						$args = array(
							'orderby' => 'name',
							'order' => 'ASC',
							'exclude' => array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'))
						);


						$categories = get_categories($args);
						?><ul><?php
							foreach($categories as $category) { ?>


								<li>
									<a href="<?php echo get_bloginfo('url');?>/category/<?php echo $category->slug;?>">
										<?php echo $category->name;?> <span class="category_post_count">(<?php echo $category->count;?>)</span>
									</a>
								</li>

							<?php }
							?>
					</div>
				</div>
				<div id="footer_contact">
					<h1>CR2 - Contracts & Revenue Recognition</h1>
					11200 Broadway #2743<br>
					Pearland, Texas 77584<br>
					832-895-6611
				</div>
				<div id="footer_social">
					<ul>
						<li><a href="https://www.facebook.com/SMARTCR2" target="_blank"><img src="<?php echo $templatePath; ?>/assets/facebook_icon.png"></a></li>
						<li><img src="<?php echo $templatePath; ?>/assets/twitter-icon.png"></li>
						<li><a href="#" target="_blank"><img src="<?php echo $templatePath; ?>/assets/google-plus-icon.png"></a></li>
						<li><img src="<?php echo $templatePath; ?>/assets/linkedin-icon.png"></li>
						<li><img src="<?php echo $templatePath; ?>/assets/Instagram-Icon.png"></li>
					</ul>

				</div>
				<div class="push"></div>
			</div>
			<script>
				$(document).ready(function() {

					$('#left-menu').sidr({
						side:'right'
					});
				});
			</script>

		</div> <!-- wrapper -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-68843819-1', 'auto');
	ga('send', 'pageview');

</script>
	</body>
</html>