<?php
/*
Template Name: LandingPage
Template Post Type: post, page, landing_page
*/
?>
<?php include(get_template_directory().'/inc/template_header.php'); ?>
<body>

<?php if ( have_posts() ) : the_post();
    $post_content = get_the_content();
    $line_01 = get_post_meta(get_the_ID(),'core_statement_line_01',true);
    $line_02 = get_post_meta(get_the_ID(),'core_statement_line_02',true);
    $tag_line = get_post_meta(get_the_ID(),'tag_line',true);
endif ?>


<div id="banner">
    <img id="cr2_splat" src="<?php echo $templatePath ?>/assets/logo.png" class="f_left">
    <h3 id="questions" class="f_right">Questions? Call us at (832)895-6611</h3>
    <div class="f_left" id = "core_container">
        <h1  id="core_statement"><?php     echo $line_01; ?><br/><?php     echo $line_02; ?></h1>
        <div id="done_container">
            <p id="steps_done" class="f_left"><?php     echo $tag_line; ?></p><img src="<?php echo $templatePath ?>/assets/done.png" id="done_graphic">
        </div>
    </div>
</div>
<div class="push"></div>

<?php     echo $post_content; ?>


<div id="form_outer_container" class="f_right">
    <div id="form_container">
        <h2>Experience CR2!</h2>
        <?php echo do_shortcode( '[contact-form-7 id="566" title="lp_software_contact"]' ); ?>
    </div>
</div>
<div class="push"></div>
<div  id="privacy_statement">*By submitting this form, you are confirmation that you have read and agree to our <a href="<?php bloginfo('url');?>/smart-cr2-privacy-policy">Terms and Privacy Statement</a>.</div>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68843819-1', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
