<?php
if (have_comments()) {

    $args = array(
        'max_depth'=> 4,
        'short_ping'  => true,
        'avatar_size' => 23,
        'reverse_top_level'=>true,
        'reverse_children'=>false
    );

    ?>
    <ol class="comment-list">
        <?php
        wp_list_comments( $args) ;
        ?>
    </ol><!-- .comment-list -->
<?php }
?>

<?php comment_form();?>
