<?php
include('header.php');

?>
    <div id='page'>
        <?php include(get_template_directory().'/inc/left-sidebar.php'); ?>

        <div id="page_middle">
            <?php if ( have_posts() ) : the_post() ?>
                <div id="single_post">
                    <div class='page-title'>
                        <?php the_title() ?>
                    </div> <!-- page-title -->
                    <div class="post_author">
                        <a href="<?php echo get_bloginfo('url');?>/author/<?php echo the_author_meta('user_login'); ?>"><?php the_author();?></a>
                    </div>
                    <div class="post_date"><?php echo get_the_date(); ?></div>


                    <div class="push"></div>
                    <div class='page-content'>
                        <?php the_content() ?>
                    </div>
                </div>

            <?php endif ?>
            <?php $categories = get_the_category();

            $booTrip = false;

            if ( ! empty( $categories ) ) {
                foreach( $categories as $category ) {
                    if ($category->name == "Quiz") {
                        $booTrip = true;
                    }
                }
            }

            ?>

            <?php
            if (!$booTrip) {
                comments_template();
            }
            ?>

        </div>

        <?php include(get_template_directory().'/inc/right-sidebar.php'); ?>


    </div> <!-- page -->
<?php
include('footer.php');

?>