<?php
include('header.php');

?>
	<div id='page'>
		<?php include(get_template_directory().'/inc/left-sidebar.php'); ?>

		<div id="page_middle">
	<?php if ( have_posts() ) : the_post() ?>
			<div class='page-title'>
				<?php the_title() ?>
			</div> <!-- page-title -->
			<div class='page-content'>
				<?php the_content() ?>
			</div> <!-- page-content -->
	<?php endif ?>

<?php
global $post;
$pagename = $post->post_name;

if ($pagename == 'product') {
	include(get_template_directory().'/inc/feature-set.php');
}
if ($pagename == 'services') {
	include(get_template_directory().'/inc/services.php');
}
if ($pagename == 'blog') {
	include(get_template_directory().'/inc/blog-page.php');
}

?>


		</div>

		<?php include(get_template_directory().'/inc/right-sidebar.php'); ?>

		<div id="more_info_responsive">
			<div id="recent_posts_responsive">
				<h1>Recent Articles</h1>

				<?php
				//Create array and fill it initially with category id of product-feature
				$notUs = array(get_category_id_by_slug('product-feature'),get_category_id_by_slug('service-area'));

				//As we add categories that we want to exclude, use this next line to add to array.
				//array_push($notUs,get_category_id_by_slug('product-feature'));

				$query = new WP_Query( array( 'category__not_in' => $notUs,'posts_per_page' => 5 ) );
				if($query->have_posts()) {
					while ( $query->have_posts() ) {
						$query->the_post(); ?>
						<div class="blog_post"><a href="<?php the_permalink();
							?>">
								<h1><?php the_title();?></h1>
								<div class="post_date"><?php the_date('m-d-Y', '<h2>', '</h2>');?></div>
								<div class="post_author"><?php the_author();?></div>

								<div class="post_excerpt"><?php the_excerpt();?></div>
							</a>
						</div>

					<?php					}
				}
				?>

			</div>

		</div>


	</div> <!-- page -->
<?php
include('footer.php');

?>