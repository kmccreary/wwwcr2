<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>SMART CR2 - Contracts and Revenue Recognition</title>
		
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>
		<?php include(get_template_directory().'/inc/helpers.php'); ?>
		<link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_small.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_medium.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo $templatePath ?>/assets/favicon_large.png" sizes="96x96">
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/reset.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/footer.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/header.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/banner.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/left-sidebar.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/right-sidebar.css'>
		<link rel="stylesheet" href="<?php echo $templatePath;?>/css/jquery.sidr.dark.css">
		<link rel="stylesheet" href="<?php echo $templatePath;?>/css/mobile-nav.css">

		<?php if ( is_front_page() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/front-page.css'>
			<title>Front Page Baby!</title>
		<?php elseif ( is_page() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/page.css'> 

			<title><?php echo (single_post_title()) ?></title>
		<?php elseif ( is_single() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/page.css'>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/comments.css'>
			<title><?php echo (single_post_title()) ?></title>
		<?php elseif ( is_404() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/page.css'>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/404.css'>
			<title>404: Content Not Found</title>
		<?php elseif ( is_category() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/category.css'>
			<title><?php single_cat_title('Archive: ') ?></title>
		<?php elseif ( is_author() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/author.css'>
			<title><?php single_cat_title('Archive: ') ?></title>
		<?php else : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/page.css'>
			<title> Generic Title </title>
		<?php endif;?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">		</script>


		<?php if(!is_front_page()) {?>

			<script>
				$(document).ready(function() {
					var collapsed = true;
					$('#more_info').click(function() {
						$('#page_middle').animate({width:'44%'},200);
						$('#page_left').animate({width:'25%'},200);
						$('#left_container').delay(200).slideDown(200);
						$('#close_more_info').delay(200).css('display','inherit');
						$(this).css('display','none');
					});
					$('#close_more_info').click(function() {
						$('#page_middle').animate({width:'64%'});
						$('#page_left').animate({width:'5%'});
						$('#left_container').css('display','none');
						$('#more_info').delay(200).css('display','inherit');
						$(this).css('display','none');
						collapsed = !collapsed;
					});

					//remove width styles when page left is hidden
					$(window).resize(function() {

						if($('#page_left').css('display') == 'none') {
							$('#page_middle').removeAttr('style');
							$('#page_left, #page_left > div').removeAttr('style');

						}

					});
				});
			</script>

		<?php } ?>

		<?php
		global $post;
		if ($post->post_name == 'product') { ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/feature-set.css'>
		<?php
		}
		if ($post->post_name == 'services') { ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/services.css'>
			<?php
		}
		?>


		<link href='https://fonts.googleapis.com/css?family=Oswald|Lora:400,400italic|Raleway:200,100,300,400,600' rel='stylesheet' type='text/css'>

		<?php /* grab the js for the slide in menu */ ?>
		<script src="<?php echo $templatePath;?>/js/jquery.sidr.min.js"></script>
<?php wp_head();?>
	</head>

	<body>
		<div id='wrapper'> 
			<div id='header'>

				<?php if(is_front_page()) {
					include("banner.php");
				} else {
					include("banner.php");
				}
	?>
			</div> <!-- header -->