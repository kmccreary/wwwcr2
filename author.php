<?php
include('header.php');

?>
<div id='page'>
    <?php include(get_template_directory().'/inc/left-sidebar.php'); ?>

    <div id="page_middle">
        <h1><?php the_author_meta('display_name',get_query_var('author'));?></h1>
        <?php if ( have_posts() ) {
            while (have_posts()) {
                the_post() ?>
                <div class="single_post">
                        <div class='post-title'>
                            <?php the_title() ?>
                        </div> <!-- page-title -->
                        <div class="author_image_small">
                            <img src="<?php echo $templatePath ?>/assets/<?php echo get_userdata($post->post_author)->user_login;?>.jpg">
                        </div>
                        <div class="post_date"><?php echo get_the_date(); ?></div><div class="post_author"><?php the_author();?></div>
                        <div class="push"></div>
                        <div class='post-excerpt'>
                            <?php the_excerpt();?>
                        </div>
                        <div class="read_more"><a href="<?php the_permalink(); ?>">Read More</a></div>

                </div>
                <?php
            }
        }
        ?>




    </div>

    <?php include(get_template_directory().'/inc/right-sidebar.php'); ?>


</div> <!-- page -->
<?php
include('footer.php');

?>